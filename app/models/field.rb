class Field < ApplicationRecord
  has_many :doctors, dependent: :destroy
  validates :area, length: {maximum: 40}, presence: true
  default_scope -> {order(area: :asc)}
end
