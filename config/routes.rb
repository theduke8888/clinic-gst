Rails.application.routes.draw do
	get 				'login', 			    			to: 'sessions#new'
	post 				'login',		    				to: 'sessions#create'
	delete 				'logout', 		        			to: 'sessions#destroy'

	root                                      				'static_pages#index'
	get                 'appointments_coming',              to: 'static_pages#appointments_coming'
	get                 'appointments_today',                to: 'static_pages#appointments_today'

	get 				'contact',                          to: 'static_pages#contact'
	get					'fields',                           to: 'fields#index'
	get 				'doctors',                          to: 'doctors#index'
	get 				'users',                            to: 'users#index'

	get 				'doctor_login',		           		to: 'sessions#new_doctor'
	post 				'doctor_login',		          		to: 'sessions#create_doctor'
	delete				'doctor_logout', 		    		to: 'sessions#destroy_doctor'

	get					'new_appointment',			    	to: 'appointments#new'
	get                 'welcome',                          to: 'appointments#welcome'
	get                 'list_doctors',                     to: 'appointments#list_doctors'

	get                 'today_appointments',               to: 'doctors#show_today_appointments'
	get                 'coming_appointments',              to: 'doctors#show_coming_appointments'
	get                 'all_appointments',                 to: 'doctors#show_all_appointments'
	resources :gibes, :fields, :doctors, :users, :appointments
end
