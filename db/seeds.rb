User.create!(
	   name:					"Gibe S. Tirol",
	   email:					"gibectu@gmail.com",
	   contact: 				Faker::PhoneNumber.cell_phone,
	   address:    			Faker::Address.full_address,
	   password:				"123456",
	   password_confirmation:	"123456",
	   admin: 				true
)

Field.create!(area: "Dermatology")
Field.create!(area: "Internal Medicine")
Field.create!(area: "Ophthalmology")
Field.create!(area: "Family Medicine")
Field.create!(area: "Pediatrics")
Field.create!(area: "Obstetricians and Gynecologists")
Field.create!(area: "Physical Medicine and Rehabilitation")

10.times do |n|
	name                	= Faker::Name.name
	license             	= Faker::Medical::NPI.npi
	email		    		= "doctor-#{n+1}@myclinic.com"
     contact             	= Faker::PhoneNumber.cell_phone
	address             	= Faker::Address.full_address
	field_id            	= rand(1..7)
	password				= "123456"
	password_confirmation    = "123456"
	Doctor.create!(
			    name:                	name,
			    license:             	license,
			    email:               	email,
			    contact:             	contact,
			    address:             	address,
			    field_id:            	field_id,
			    password: 				password,
			    password_confirmation:	password_confirmation
	)
end

10.times do |n|
	name                	= Faker::Name.name
	email               	= "user-#{n+1}@myclinic.com"
	contact            	 	= Faker::PhoneNumber.cell_phone
	address             	= Faker::Address.full_address
	password				= "123456"
	password_confirmation    = "123456"
	User.create!(
			  name:                  	name,
			  email:                 	email,
			  contact:               	contact,
			  address:               	address,
			  password: 				password,
			  password_confirmation:      password_confirmation
	)
end

100.times do |n|
	name                        = Faker::Name.name
	address                     = ["Cebu City", "Mandaue", "Lapulapu", "Talisay", "Minglanilla"].sample
	email                       = Faker::Internet.email
	contact                     = Faker::PhoneNumber.cell_phone
	dob                         = Faker::Date.birthday
	doa                         = [DateTime.now, Faker::Date.forward].sample
	job                         = Faker::Job.position
	gender                      = %w(Male Female).sample
	doctor_id                   = rand(1..10)
	Appointment.create!(
			name:               name,
			address:            address,
			email:              email,
			contact:            contact,
			dob:                dob,
			doa:                doa,
			gender:             gender,
			doctor_id:          doctor_id,
			job:                job
	)
end


