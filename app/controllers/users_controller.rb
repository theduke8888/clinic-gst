class UsersController < ApplicationController
	include SessionsHelper
	before_action :check_logged_in
#			before_action :check_user_status, only: [:destroy]
	before_action :check_doctor_status
	before_action :correct_user, only: [:edit, :update]
	before_action :admin_user, only:  [:destroy]



	def index
		#@users = User.all
		@users = User.order(:name).page(params[:page]).per(15)
	end

	def show
		@user = User.find(params[:id])
	end

	def new
		@user = User.new
	end

	def create
		@user = User.new(params_user)
		if @user.save
			flash[:success] = "New user added"
			redirect_to users_url
		else
			render 'new'
		end
	end

	def edit
		@user = User.find(params[:id])
	end

	def update
		@user = User.find(params[:id])
		if @user.update_attributes(params_user)
			flash[:success] = "User profile updated"
			redirect_to users_url
		else
			render 'edit'
		end
	end

	def destroy
		User.find(params[:id]).delete
		flash[:success]  = "User successfully deleted"
		redirect_to users_url
	end

	private

		def params_user
			params.require(:user).permit(:name, :email, :contact, :address, :password, :password_confirmation )
		end

		def check_user_status
			if  logged_in?
				redirect_to root_url
			end
		end

		def check_doctor_status
			if logged_in_doctor?
				redirect_to root_url
			end
		end

end
