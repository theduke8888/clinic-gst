class SessionsController < ApplicationController
	before_action :check_for_logged_again, except: [:create, :destroy, :destroy_doctor, :create_doctor]

	def new

	end

	def create
		user = User.find_by(email: params[:session][:email].downcase)
		if user && user.authenticate(params[:session][:password])
			log_in(user)
			params[:session][:remember_me] == '1' ? remember(user) : forget(user)
			redirect_to user
		else flash.now[:danger] = "Invalid login details"
			render 'new'

		end
	end

	def destroy
		log_out if logged_in?
		redirect_to root_url
	end

	def new_doctor

	end

	def create_doctor
		doctor = Doctor.find_by(email: params[:session][:email].downcase)
		if doctor && doctor.authenticate(params[:session][:password])
			login_in_doctor(doctor)
			params[:session][:remember_me] = '1' ? remember_doctor(doctor) : forget_doctor(doctor)
			redirect_to all_appointments_url
		else
			flash.now[:danger] = "Invalid login details"
			render 'new_doctor'
		end
	end

	def destroy_doctor
		log_out_doctor if logged_in_doctor?
		redirect_to doctor_login_url
	end

	private

		def check_for_logged_again
			if logged_in? || logged_in_doctor?
				redirect_to root_url
			end
		end

end
