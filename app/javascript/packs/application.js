// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

import confirmDatePlugin from "flatpickr/dist/plugins/confirmDate/confirmDate";

require("@rails/ujs").start()
require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")
require("jquery")
import "bootstrap"
require("bootstrap-datepicker")
require("flatpickr")
import "popper.js"

import  "flatpickr/dist/plugins/confirmDate/confirmDate"
import "./custom2"

document.addEventListener('DOMContentLoaded', function() {
    flatpickr('#dob', {
        altInput: true,
        altFormat: "F j, Y D",
        dateFormat: "Y-m-d",
        maxDate: 'today',
        plugins: [
            new confirmDatePlugin({})
        ]
    }),

    flatpickr('#doa', {
    altInput: true,
    altFormat: "F j, Y D",
    dateFormat: "Y-m-d",
    minDate: 'today',
    disable: [
        function(date) {
            // return true to disable
            return (date.getDay() === 0 || date.getDay() === 6);

        }
    ],
    locale: {
        firstDayOfWeek: 1
    },
    plugins: [
        new confirmDatePlugin({})
    ]
})


})

