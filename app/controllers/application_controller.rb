class ApplicationController < ActionController::Base
	protect_from_forgery with: :exception
	include SessionsHelper
	before_action :set_cache_headers


	protected

		def check_logged_in
			unless logged_in? || logged_in_doctor?
				if request.original_url == root_url
					redirect_to welcome_url
				elsif request.original_url == doctor_login_url
					flash[:danger] = 'You are not logged in'
					redirect_to doctor_login_url
				else
					flash[:danger] = 'You are not logged in'
					redirect_to login_url
				end
			end

		end

		def correct_user
			@user = User.find(params[:id])
			redirect_to root_url unless @user.eql?(current_user)
		end

		def admin_user
			redirect_to root_url unless current_user.admin?
		end

	   	def correct_doctor
		    #  @doctor = Doctor.find(params[:id])
			redirect_to root_url unless @doctor.eql?(current_doctor)
		end

		def set_cache_headers
			response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
			response.headers["Pragma"] = "no-cache"
			response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
		end
end
