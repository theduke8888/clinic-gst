class StaticPagesController < ApplicationController
	before_action :check_logged_in
	def index
		@doctors = Doctor.order('name').all
	end

	def contact
	end

	def appointments_coming
		@appointments = Appointment.order('doa').where('doa > ?', Time.zone.today).page(params[:page]).per(15)
	end

	def appointments_today
		@appointments = Appointment.order('name').where(doa: Time.zone.today).limit(15).page(params[:page]).per(15)
	end


end
