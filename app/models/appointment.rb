class Appointment < ApplicationRecord
	belongs_to :doctor
	default_scope  -> { order(doa: :asc) }
	validates :name, presence: true
	validates :address, presence: true
	validates :contact, presence: true
	validates :dob, presence: true
	validates :doa, presence: true
	validates :job, presence: true
	validates :doctor_id, presence: true
	validates :gender, presence: true
	validates :email, presence: true
end
