class AddEmailToAppointments < ActiveRecord::Migration[6.0]
  def change
    add_column :appointments, :email, :string
  end
end
