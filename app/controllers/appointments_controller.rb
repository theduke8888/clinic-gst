class AppointmentsController < ApplicationController

	def show
		@appointment = Appointment.find(params[:id])
	end

	def new
		@appointment = Appointment.new
	end

	def create
		@appointment = Appointment.new(appointment_params)
		if @appointment.save
			flash[:success] = "Your appointment is save";
			render 'show'
		else
			render 'new'
		end
	end

	def welcome
	end

	def list_doctors
		@doctors = Doctor.all
		@fields = Field.order('area').all
	end
	private

		def appointment_params
			params.require(:appointment).permit(:name, :address, :email, :contact, :dob, :doa, :job, :doctor_id, :gender)
		end
end


