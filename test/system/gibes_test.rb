require "application_system_test_case"

class GibesTest < ApplicationSystemTestCase
  setup do
    @gibe = gibes(:one)
  end

  test "visiting the index" do
    visit gibes_url
    assert_selector "h1", text: "Gibes"
  end

  test "creating a Gibe" do
    visit gibes_url
    click_on "New Gibe"

    fill_in "Address", with: @gibe.address
    fill_in "Name", with: @gibe.name
    click_on "Create Gibe"

    assert_text "Gibe was successfully created"
    click_on "Back"
  end

  test "updating a Gibe" do
    visit gibes_url
    click_on "Edit", match: :first

    fill_in "Address", with: @gibe.address
    fill_in "Name", with: @gibe.name
    click_on "Update Gibe"

    assert_text "Gibe was successfully updated"
    click_on "Back"
  end

  test "destroying a Gibe" do
    visit gibes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Gibe was successfully destroyed"
  end
end
